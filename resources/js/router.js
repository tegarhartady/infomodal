import Vue from 'vue'
import VueRouter from 'vue-router'

import About from './views/About.vue'
import Artikel from './views/Artikel.vue'

Vue.use(VueRouter)

const router = new VueRouter({
    mode: 'history',
    routes: [
        {
            path: '/',
            name: 'about',
            component: About
        },
        {
            path: '/artikel',
            name: 'artikel',
            component: Artikel
        }
    ]
})  

export default router