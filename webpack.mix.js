const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
    .sass('resources/sass/app.scss', 'public/css');

mix.js([
    'public/assets/vendor/jquery/jquery.min.js',
    'public/assets/vendor/bootstrap/js/bootstrap.bundle.min.js',
    'public/assets/vendor/jquery.easing/jquery.easing.min.js',
    'public/assets/vendor/php-email-form/validate.js',
    'public/assets/vendor/jquery-sticky/jquery.sticky.js',
    'public/assets/vendor/isotope-layout/isotope.pkgd.min.js',
    'public/assets/vendor/venobox/venobox.min.js',
    'public/assets/vendor/waypoints/jquery.waypoints.min.js',
    'public/assets/vendor/owl.carousel/owl.carousel.min.js',
    'public/assets/vendor/aos/aos.js',
], 'public/assets/js/app.js');
